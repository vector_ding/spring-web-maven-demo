### 打包源码工程
mvn package -DskipTests=true

### 生成docker镜像
docker build -t tomcat-war-demo-img .

#docker stop tomcat-war-demo-8080
#docker rm tomcat-war-demo-8080

### 运行Docker镜像
docker run --name tomcat-war-demo -d -p 8080:8080 tomcat-war-demo-img
